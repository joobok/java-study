package example.tree;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * TreeItem 검색 유틸. 하위 노드 전부 검색함.
 */
@Data
@Accessors(chain = true)
public class TreeItemSearch {
    public enum MatchRange {
        PATH,
        NAME,
        NAME_NO_EXT,
        EXT_NAME
    }
    public enum MatchType {
        FULL,
        PART,
        START,
        END,
        REG_EX
    }
    public enum MultiMode {
        AND,
        OR
    }
    public enum NextSearchMode {
        FILTER,
        REMOVE,
        NEW,
        APPEND
    }

    private MatchRange matchRange = MatchRange.PATH;
    private MatchType matchType = MatchType.PART;
    private MultiMode multiMode = MultiMode.OR;
    private NextSearchMode nextSearchMode = NextSearchMode.APPEND;
    private boolean caseSensitive = true;
    private boolean onlyLeaf = false;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private TreeItem treeItem;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private List<TreeItem> searchedList = new ArrayList<>();

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private List<TreeItem> filteredList = new ArrayList<>();

    public TreeItemSearch(TreeItem treeItem) {
        this.treeItem = treeItem;
    }

    public TreeItemSearch search(String... queryArr) {
        beforeSearchInitResult();
        doSearch(queryArr);
        afterSearchReadyForNextJob();
        return this;
    }

    private void beforeSearchInitResult() {
        if(nextSearchMode.equals(NextSearchMode.NEW)) {
            searchedList = new ArrayList<>();
        }
    }

    private void doSearch(String... queryArr) {
        List<String> validQueryList = Arrays.stream(queryArr).filter(StringUtils::isNotEmpty).collect(Collectors.toList());
        if(queryArr.length != validQueryList.size()) throw new RuntimeException("검색어를 입력하시오.");
        recursiveTreeSearch(treeItem, validQueryList);
    }

    private void afterSearchReadyForNextJob() {
        if(nextSearchMode.equals(NextSearchMode.FILTER)) {
            searchedList = filteredList;
            filteredList = new ArrayList<>();
        }
        if(onlyLeaf) {
            searchedList = searchedList.stream().filter(TreeItem::isLeaf).collect(Collectors.toList());
        }
    }

    private void recursiveTreeSearch(TreeItem treeItem, List<String> queryList) {
        if(isListMatch(treeItem, queryList)) {
            switch (nextSearchMode) {
                case FILTER: if(searchedList.contains(treeItem)) filteredList.add(treeItem); break;
                case REMOVE: searchedList.remove(treeItem); break;
                case NEW:
                case APPEND: searchedList.add(treeItem); break;
            }
        }
        if(!treeItem.isLeaf()) {
            treeItem.getChildren().forEach(item -> recursiveTreeSearch(item, queryList));
        }
    }

    private boolean isListMatch(TreeItem treeItem, List<String> queryList) {
        boolean isAnyMatch = multiMode.equals(MultiMode.OR)  && queryList.stream().anyMatch(query -> isMatch(treeItem, query));
        boolean isAllMatch = multiMode.equals(MultiMode.AND) && queryList.stream().allMatch(query -> isMatch(treeItem, query));
        return isAnyMatch || isAllMatch;
    }

    private boolean isMatch(TreeItem treeItem, String query) {
        String valueToMatch = prepareValueToMatch(treeItem);
        String queryToMatch = prepareQuery(query);
        boolean result = false;
        switch (matchType) {
            case FULL:   result = valueToMatch.equals(queryToMatch); break;
            case PART:   result = valueToMatch.contains(queryToMatch); break;
            case START:  result = valueToMatch.startsWith(queryToMatch); break;
            case END:    result = valueToMatch.endsWith(queryToMatch); break;
            case REG_EX: result = Pattern.matches(queryToMatch, valueToMatch); break;
        }
        return result;
    }

    private String prepareValueToMatch(TreeItem treeItem) {
        String value = "";
        switch (matchRange) {
            case PATH:        value = treeItem.getPath(); break;
            case NAME:        value = treeItem.getName(); break;
            case NAME_NO_EXT: value = FilenameUtils.removeExtension(treeItem.getName()); break;
            case EXT_NAME:    value = FilenameUtils.getExtension(treeItem.getName()); break;
        }
        if(!caseSensitive) {
            value = value.toLowerCase();
        }
        return value;
    }

    private String prepareQuery(String query) {
        if(!caseSensitive && !matchType.equals(MatchType.REG_EX)) {
            query = query.toLowerCase();
        }
        return query;
    }

    public List<TreeItem> getResult() {
        return searchedList;
    }
}
