package example.tree;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.*;
import java.util.regex.Pattern;

/**
 * 파일, 조직도, 메뉴, 카테고리 등 계층 구조의 데이터를 단순 표현하기 위한 용도의 공통 클래스.
 */
@Data
@Accessors(chain=true)
public class TreeItem {
    private String pathDelim = ">";
    private String path = UUID.randomUUID().toString();
    private String name;
    private TreeItem parent;
    private List<TreeItem> children = new ArrayList<TreeItem>();
    private boolean leaf;
    private Map<String, Object> props;

    public TreeItem(String path) {
        setPath(path);
    }

    public void setParent(TreeItem parent) {
        if(getParent() != null) {
            getParent().removeChild(this);
        }
        parent.addChild(this);
        this.parent = parent;
    }

    private void removeChild(TreeItem child) {
        getChildren().remove(child);
    }

    private void addChild(TreeItem child) {
        getChildren().add(child);
    }

    public TreeItem addProp(String name, Object value) {
        if(getProps() == null) {
            setProps(new HashMap());
        }
        getProps().put(name, value);
        return this;
    }

    private String[] splitPath() {
        return path.split(Pattern.quote(pathDelim));
    }

    public int getLevel() {
        return splitPath().length;
    }

    public String getName() {
        String[] split = splitPath();
        return split[split.length - 1];
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TreeItem treeItem = (TreeItem) o;
        return path.equals(treeItem.path);
    }

    @Override
    public int hashCode() {
        return getPath().hashCode();
    }

    /**
     * 롬복이 생성하는 toString 은 모든 필드 값을 가져와 리턴하도록 되어있다.
     * TreeItem 은 멤버로 부모와 자식을 다 가지고 있으므로,
     * 롬복이 생성한 toString 을 그대로 사용하면 부모와 자식이 서로 콜하면서 무한루프에 빠지게 된다.
     * 롬복이 toString 을 생성하지 않도록 옵션을 줄 수 있지만 여기선 간단히 오버라이드 함.
     */
    @Override
    public String toString() {
        return this.getPath();
    }
}
