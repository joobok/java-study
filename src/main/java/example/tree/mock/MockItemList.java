package example.tree.mock;

import java.util.Arrays;
import java.util.List;

public class MockItemList {
    public static List<MockItem> getMockItemList() {
        return Arrays.asList(
            new MockItem("0","식품", null),
            new MockItem("3","홍삼","1"),
            new MockItem("6","빵","5"),
            new MockItem("4","건강즙","1"),
            new MockItem("7","과자","5"),
            new MockItem("8","아이스크림","5"),
            new MockItem("2","영양제","1"),
            new MockItem("1","건강식품","0"),
            new MockItem("5","간식","0")
        );
    }
}
