package example.tree.mock;

import lombok.Data;

/**
 * 트리 구조 데이터를 저장하기 위한 흔한 DB 테이블 형태를 모방.
 */
@Data
public class MockItem {
    private String id;
    private String name;
    private String parentId;
    public MockItem(String id, String name, String parentId) {
        setId(id);
        setName(name);
        setParentId(parentId);
    }
}
