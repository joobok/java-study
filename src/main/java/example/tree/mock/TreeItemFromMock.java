package example.tree.mock;

import example.tree.TreeItem;
import example.tree.converter.TreeItemSupplier;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TreeItemFromMock implements TreeItemSupplier<List<MockItem>> {
    private Map<String, List<MockItem>> groupByParent;

    @Override
    public TreeItem from(List<MockItem> mockItems) {
        groupByParent = mockItems.stream().collect(Collectors.groupingBy(item -> StringUtils.defaultString(item.getParentId(), "ROOT")));
        return from(new MockItem("ROOT","ROOT",null), new TreeItem("ROOT"));
    }

    private TreeItem from(MockItem currentMockItem, TreeItem currentTree) {
        List<MockItem> children = groupByParent.get(currentMockItem.getId());
        currentTree.setLeaf(children == null);
        copy(currentMockItem, currentTree);
        if(children != null) {
            for(MockItem child : children) {
                TreeItem newChild = new TreeItem(child.getId());
                newChild.setPath(child.getName());
                newChild.setParent(currentTree);
                from(child, newChild);
            }
        }
        return currentTree;
    }

    private void copy(MockItem from, TreeItem to) {
        to.setPathDelim(" > ");
        to.setPath(appendParentPath(to.getPath(), to));
        to.addProp("name", from.getName());
    }

    private String appendParentPath(String currentPath, TreeItem currentItem) {
        TreeItem parent = currentItem.getParent();
        if(parent != null) {
            currentPath = parent.getPath() + currentItem.getPathDelim() + currentPath;
            appendParentPath(currentPath, parent);
        }
        return currentPath;
    }

}
