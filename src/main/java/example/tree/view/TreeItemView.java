package example.tree.view;

import example.tree.TreeItem;
import example.tree.converter.TreeItemConverter;

public interface TreeItemView {
    void render(TreeItem treeItem, TreeItemConverter treeItemConverter);
}
