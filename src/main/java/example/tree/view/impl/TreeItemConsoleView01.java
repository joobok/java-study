package example.tree.view.impl;

import example.tree.TreeItem;
import example.tree.converter.TreeItemConverter;
import example.tree.view.TreeItemView;
import lombok.Data;

/**
 * 콘솔 창에 순서대로 프린트
 */
@Data
public class TreeItemConsoleView01 implements TreeItemView {
    private TreeItemConverter treeItemConverter;
    public void render(TreeItem treeItem, TreeItemConverter treeItemConverter) {
        setTreeItemConverter(treeItemConverter);
        render(treeItem);
    }
    private void render(TreeItem currentTree) {
        System.out.println(getTreeItemConverter().convert(currentTree));
        if(!currentTree.isLeaf()) {
            for(TreeItem child : currentTree.getChildren()) {
                render(child);
            }
        }
    }
}
