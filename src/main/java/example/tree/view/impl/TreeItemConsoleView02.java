package example.tree.view.impl;

import example.tree.TreeItem;
import example.tree.converter.TreeItemConverter;
import example.tree.view.TreeItemView;
import lombok.Data;

import java.util.Comparator;

/**
 * 콘솔 창에 폴더 우선 프린트
 */
@Data
public class TreeItemConsoleView02 implements TreeItemView {
    private TreeItemConverter treeItemConverter;
    public void render(TreeItem treeItem, TreeItemConverter treeItemConverter) {
        setTreeItemConverter(treeItemConverter);
        render(treeItem);
    }
    private void render(TreeItem currentTree) {
        System.out.println(getTreeItemConverter().convert(currentTree));
        if(!currentTree.isLeaf()) {
            currentTree.getChildren().sort(new Comparator<TreeItem>() {
                public int compare(TreeItem o1, TreeItem o2) {
                    int result = Boolean.valueOf(o1.isLeaf()).compareTo(Boolean.valueOf(o2.isLeaf()));
                    if(result == 0) {
                        result = o1.getPath().compareTo(o2.getPath());
                    }
                    return result;
                }
            });
            for(TreeItem child : currentTree.getChildren()) {
                render(child);
            }
        }
    }
}
