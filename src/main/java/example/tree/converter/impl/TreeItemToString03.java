package example.tree.converter.impl;

import example.tree.TreeItem;
import example.tree.converter.TreeItemConverter;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Pattern;

/**
 * TreeItem 의 레벨(부모 폴더의 갯수) * 2 개만큼 앞에 공백을 붙여서 name 을 리턴
 */
public class TreeItemToString03 implements TreeItemConverter<String> {
    private int rootDepth = -1;
    public String convert(TreeItem treeItem) {
        int depth = treeItem.getLevel();
        if(rootDepth == -1) {
            rootDepth = depth;
        }
        return StringUtils.repeat(" ", (depth - rootDepth)*2) + treeItem.getName();
    }
}
