package example.tree.converter.impl;

import example.tree.TreeItem;
import example.tree.converter.TreeItemSupplier;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * File 을 TreeItem 으로 변환 제공. 부모 자식 관계를 그대로 복사함.
 */
public class TreeItemFromFile implements TreeItemSupplier<File> {
    @Override
    public TreeItem from(File file) {
        if(!file.exists()) throw new RuntimeException("File not found : " + file.getName());
        return from(file, new TreeItem(file.getAbsolutePath()));
    }

    private TreeItem from(File file, TreeItem currentTree) {
        copy(file, currentTree);
        if(file.isDirectory()) {
            File[] children = file.listFiles();
            for(File childFile : children) {
                TreeItem newChildTree = new TreeItem(childFile.getAbsolutePath());
                newChildTree.setParent(currentTree);
                from(childFile, newChildTree);
            }
        }
        return currentTree;
    }

    private void copy(File from, TreeItem to) {
        to.setPathDelim(File.separator)
          .setLeaf(from.isFile())
          .addProp("size", from.length())
          .addProp("hidden", from.isHidden())
          .addProp("canRead", from.canRead())
          .addProp("canWrite", from.canWrite());
    }
}