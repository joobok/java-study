package example.tree.converter.impl;

import example.tree.TreeItem;
import example.tree.converter.TreeItemConverter;

/**
 * TreeItem 의 path 를 리턴
 */
public class TreeItemToString01 implements TreeItemConverter<String> {
    public String convert(TreeItem treeItem) {
        return treeItem.getPath();
    }
}
