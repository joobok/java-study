package example.tree.converter;

import example.tree.TreeItem;

/**
 * 지정한 타입의 객체를 TreeItem 으로 변환 제공.
 */
public interface TreeItemSupplier<T> {
    TreeItem from(T t);
}
