package example.tree.converter;

import example.tree.TreeItem;

/**
 * 특정 TreeItem 을 지정한 타입으로 변환.
 */
public interface TreeItemConverter<T> {
    T convert(TreeItem treeItem);
}
