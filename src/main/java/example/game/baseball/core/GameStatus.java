package example.game.baseball.core;

public enum GameStatus {
    WIN,
    LOSE,
    CONTINUE
}
