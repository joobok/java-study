package example.game.baseball.core;

import lombok.Data;

import java.util.List;

@Data
public class MatchingResult {
    private int currentRound;
    private List<Integer> userInput;
    private int strike;
    private int ball;

    public MatchingResult(List<Integer> userInput) {
        setUserInput(userInput);
    }

    public void strike() {
        ++strike;
    }
    public void ball() {
        ++ball;
    }
}
