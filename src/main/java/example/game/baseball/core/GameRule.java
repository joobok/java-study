package example.game.baseball.core;

import example.game.baseball.common.GameException;
import example.game.baseball.common.Messages;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.RandomUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Data
@Accessors(chain = true)
public class GameRule {
    /** 매칭할 숫자 최대 값 */
    private int maxValue = 9;
    /** 매칭할 숫자 최대 갯수 */
    private int matchingSize = 4;
    /** 최대 매칭 횟수 */
    private int maxMatchingRound = 10;
    /** 입력값 밸리데이터 */
    private List<Consumer<List<Integer>>> inputValidators = Arrays.asList(inputList -> {
        if(inputList.size() != matchingSize) throw new GameException(Messages.MAX_SIZE_OVER);
        if(inputList.stream().anyMatch(value -> value < 1 || value > maxValue)) throw new GameException(Messages.MAX_VALUE_OVER);
        if(inputList.size() != inputList.stream().distinct().collect(Collectors.toList()).size()) throw new GameException(Messages.DUP_VALUE);
    });
    /** 필요시 밸리데이터 추가 */
    public void addInputValidators(Consumer<List<Integer>> validator) {
        getInputValidators().add(validator);
    }
    /** 랜덤 숫자 생성 */
    public List<Integer> getDefender() {
        List<Integer> defender = new ArrayList<>();
        while(defender.size() < matchingSize) {
            Integer nextInt = RandomUtils.nextInt(1, maxValue + 1);
            if(!defender.contains(nextInt)) {
                defender.add(nextInt);
            }
        }
        return defender;
    }
    /** 현재 라운드 매칭 결과 */
    public MatchingResult match(List<Integer> defender, List<Integer> userInput) {
        MatchingResult matchingResult = new MatchingResult(userInput);
        for(int n=0; n<defender.size(); n++) {
            for(int x=0; x<userInput.size(); x++) {
                if(defender.get(n).equals(userInput.get(x))) {
                    if(n==x) {
                        matchingResult.strike();
                    }else{
                        matchingResult.ball();
                    }
                }
            }
        }
        return matchingResult;
    }
    /** 게임 승/패/계속진행 판단 */
    public GameStatus getResult(MatchingResult matchingResult) {
        if(matchingResult.getStrike() == matchingSize) {
            return GameStatus.WIN;
        }else if(matchingResult.getCurrentRound() >= maxMatchingRound) {
            return GameStatus.LOSE;
        }else{
           return GameStatus.CONTINUE;
        }
    }
}
