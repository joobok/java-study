package example.game.baseball.common;

public class GameException extends RuntimeException {
    public GameException(String message) {
        super(message);
    }

    public GameException(Exception e) {
        super(e);
    }
}
