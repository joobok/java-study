package example.game.baseball.common;

public class Messages {
    public static final String REQUIRE_NUMBER = "숫자만 입력해 주십시오.";
    public static final String CONSOLE_INPUT_RULE_INFO = "1 이상 %s 이하의 숫자 %s 개를 %s 구분하여 입력해 주세요. (%s 회 시도 제한)";
    public static final String MAX_SIZE_OVER = "입력할 숫자 갯수가 맞지 않습니다.";
    public static final String MAX_VALUE_OVER = "입력 가능한 숫자 최소/최대값이 맞지 않습니다.";
    public static final String DUP_VALUE = "중복된 숫자가 있습니다.";
    public static final String MATCHING_RESULT = "%s 볼 %s 스트라이크";
    public static final String WIN_MESSAGE = "WIN!";
    public static final String LOSE_MESSAGE = "LOSE!";
    public static final String ASK_RESTART_CONSOLE = "한 번 더? (하려면 %s 입력)";
    public static final String ASK_RESTART_SWING = "한 번 더?";
}