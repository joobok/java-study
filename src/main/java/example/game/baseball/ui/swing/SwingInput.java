package example.game.baseball.ui.swing;

import example.game.baseball.common.GameException;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import javax.swing.*;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

@Data
public class SwingInput {
    private List<Consumer<List<Integer>>> validators;
    private Function<String, List<Integer>> converter;
    public SwingInput(Function<String, List<Integer>> converter, List<Consumer<List<Integer>>> validators) {
        setConverter(converter);
        setValidators(validators);
    }
    public List<Integer> getInput(JLabel validationMsg, JTextField field) {
        List<Integer> result = null;
        while(result == null) {
            String text = field.getText();
            try {
                List<Integer> input = converter.apply(field.getText());
                validators.forEach(validator -> validator.accept(input));
                result = input;
            } catch (GameException ge) {
                validationMsg.setText(StringUtils.isNotEmpty(text) ? ge.getMessage() : "");
            }
        }
        validationMsg.setText("");
        return result;
    }
}
