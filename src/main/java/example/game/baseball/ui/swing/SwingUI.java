package example.game.baseball.ui.swing;

import example.game.baseball.common.GameException;
import example.game.baseball.ui.GameUI;
import example.game.baseball.common.Messages;
import example.game.baseball.core.GameRule;
import example.game.baseball.core.MatchingResult;
import org.apache.commons.lang3.StringUtils;

import javax.swing.*;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

/**
 * 단순 테스트용으로 아주 대충 만든 게임 UI
 */
public class SwingUI implements GameUI {

    private final String delim = "\\s";
    private final String delimDesc = "공백으로";

    private JFrame frame = new JFrame();
    private JLabel ruleInfo = new JLabel();
    private JTextField userInput = new JTextField(20);
    private JLabel validateMsg = new JLabel();
    private JLabel resultList = new JLabel();

    /** 콤마로 구분한 문자열을 숫자 리스트로 변환 */
    private final Function<String, List<Integer>> STR_TO_INT_LIST = input -> {
        String[] split = StringUtils.defaultString(input).split(delim);
        return Arrays.asList(split).stream().map(value -> {
            try {
                return Integer.valueOf(value.trim());
            } catch (NumberFormatException nfe) {
                throw new GameException(Messages.REQUIRE_NUMBER);
            }
        }).collect(Collectors.toList());
    };

    public SwingUI() {
        frame.setTitle("야구 게임");

        JPanel panel = new JPanel();
        panel.add(ruleInfo);
        userInput.setBounds(50,50,200,50);
        panel.add(userInput);
        panel.add(validateMsg);
        panel.add(resultList);

        frame.add(panel);
        frame.setSize(500,300);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    @Override
    public List<Integer> getUserInput(GameRule gameRule) {
        ruleInfo.setText(String.format(Messages.CONSOLE_INPUT_RULE_INFO, gameRule.getMaxValue(), gameRule.getMatchingSize(), delimDesc, gameRule.getMaxMatchingRound()));
        userInput.setText("");
        SwingInput swingInput = new SwingInput(STR_TO_INT_LIST, gameRule.getInputValidators());
        return swingInput.getInput(validateMsg, userInput);
    }

    @Override
    public void reportRoundResult(List<MatchingResult> matchingResults) {
        String roundResult = matchingResults.stream().map(matchingResult -> getResult(matchingResult)).collect(Collectors.joining(System.lineSeparator()));
        resultList.setText("<html><pre>" + roundResult + "</pre></html>");
    }


    private String getResult(MatchingResult matchingResult) {
        return matchingResult.getCurrentRound() + " 번째 시도 : " + matchingResult.getUserInput().toString() + " ==> " + String.format(Messages.MATCHING_RESULT, matchingResult.getBall(), matchingResult.getStrike());
    }

    @Override
    public void reportGameResult(String msg) {
        JOptionPane.showMessageDialog(frame, msg);
    }

    @Override
    public boolean askRestart() {
        int yesOrNo = JOptionPane.showConfirmDialog(frame, Messages.ASK_RESTART_SWING, "Confirm", JOptionPane.YES_NO_OPTION);
        return yesOrNo == JOptionPane.YES_OPTION;
    }

    @Override
    public void finish() {
        System.exit(0);
    }
}
