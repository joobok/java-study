package example.game.baseball.ui;

import example.game.baseball.core.GameRule;
import example.game.baseball.core.MatchingResult;

import java.util.List;

public interface GameUI {
    List<Integer> getUserInput(GameRule gameRule);
    void reportRoundResult(List<MatchingResult> matchingResults);
    void reportGameResult(String msg);
    boolean askRestart();
    void finish();
}
