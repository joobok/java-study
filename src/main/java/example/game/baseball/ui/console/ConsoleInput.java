package example.game.baseball.ui.console;

import example.game.baseball.common.GameException;
import lombok.Data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

@Data
public class ConsoleInput {
    private String exit = "exit";
    private List<Consumer<List<Integer>>> validators;
    private Function<String, List<Integer>> converter;
    public ConsoleInput(Function<String, List<Integer>> converter, List<Consumer<List<Integer>>> validators) {
        setConverter(converter);
        setValidators(validators);
    }
    public List<Integer> getInput(String msg) {
        try {
            System.out.println(msg);
            String input = new BufferedReader(new InputStreamReader(System.in)).readLine();
            if(exit.equalsIgnoreCase(input)) System.exit(0);
            List<Integer> result = converter.apply(input);
            validators.forEach(validator -> validator.accept(result));
            return result;
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (GameException ge) {
            System.out.println(ge.getMessage());
            return getInput(msg);
        }
    }
    public static String getYn(String msg) {
        try {
            System.out.println(msg);
            return new BufferedReader(new InputStreamReader(System.in)).readLine();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
