package example.game.baseball.ui.console;

import example.game.baseball.common.GameException;
import example.game.baseball.common.Messages;
import example.game.baseball.core.GameRule;
import example.game.baseball.core.MatchingResult;
import example.game.baseball.ui.GameUI;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ConsoleUI implements GameUI {

    private final String delim = ",";
    private final String delimDesc = "콤마로(,)";

    /** 콤마로 구분한 문자열을 숫자 리스트로 변환 */
    private final Function<String, List<Integer>> STR_TO_INT_LIST = input -> {
        String[] split = StringUtils.defaultString(input).split(delim);
        return Arrays.asList(split).stream().map(value -> {
            try {
                return Integer.valueOf(value.trim());
            } catch (NumberFormatException nfe) {
                throw new GameException(Messages.REQUIRE_NUMBER);
            }
        }).collect(Collectors.toList());
    };

    @Override
    public List<Integer> getUserInput(GameRule gameRule) {
        ConsoleInput consoleInput = new ConsoleInput(STR_TO_INT_LIST, gameRule.getInputValidators());
        String ruleInfo = String.format(Messages.CONSOLE_INPUT_RULE_INFO, gameRule.getMaxValue(), gameRule.getMatchingSize(), delimDesc, gameRule.getMaxMatchingRound());
        return consoleInput.getInput(ruleInfo);
    }

    @Override
    public void reportRoundResult(List<MatchingResult> matchingResults) {
        System.out.println(
            matchingResults.stream()
                .map(matchingResult -> getResult(matchingResult))
                .collect(Collectors.joining(System.lineSeparator())));
    }

    private String getResult(MatchingResult matchingResult) {
        return matchingResult.getCurrentRound() + " 번째 시도 : " + matchingResult.getUserInput().toString() + " ==> " + String.format(Messages.MATCHING_RESULT, matchingResult.getBall(), matchingResult.getStrike());
    }

    @Override
    public void reportGameResult(String msg) {
        System.out.println(msg);
    }

    @Override
    public boolean askRestart() {
        String yes = "Y";
        return yes.equalsIgnoreCase(ConsoleInput.getYn(String.format(Messages.ASK_RESTART_CONSOLE, yes)));
    }

    @Override
    public void finish() {
        System.out.println("exit...");
        System.exit(0);
    }
}
