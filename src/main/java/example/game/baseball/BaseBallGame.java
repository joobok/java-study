package example.game.baseball;

import example.game.baseball.common.Messages;
import example.game.baseball.core.GameRule;
import example.game.baseball.core.MatchingResult;
import example.game.baseball.ui.GameUI;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class BaseBallGame {
    private GameUI gameUI;
    private GameRule gameRule;
    private List<MatchingResult> matchingResults = new ArrayList<>();

    public BaseBallGame(GameUI gameUI, GameRule gameRule) {
        setGameUI(gameUI);
        setGameRule(gameRule);
    }
    public void start() {
        matchingResults.clear();
        List<Integer> defender = gameRule.getDefender();
        System.out.println(gameUI.getClass().getSimpleName() + " : " + defender);
        play(defender);
    }

    private void play(List<Integer> defender) {
        List<Integer> userInput = gameUI.getUserInput(gameRule);
        MatchingResult matchingResult = gameRule.match(defender, userInput);
        matchingResults.add(matchingResult);
        matchingResult.setCurrentRound(matchingResults.size());
        gameUI.reportRoundResult(matchingResults);
        switch (gameRule.getResult(matchingResult)) {
            case WIN : win(); break;
            case LOSE : lose(); break;
            default : play(defender);
        }
    }

    private void win() {
        gameUI.reportGameResult(Messages.WIN_MESSAGE);
        if(gameUI.askRestart()) {
            start();
        } else {
            gameUI.finish();
        }
    }

    private void lose() {
        gameUI.reportGameResult(Messages.LOSE_MESSAGE);
        if(gameUI.askRestart()) {
            start();
        } else {
            gameUI.finish();
        }
    }
}
