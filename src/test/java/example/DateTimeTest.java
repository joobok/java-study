package example;

import org.junit.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateTimeTest {
    @Test
    public void formatTest() {
        DateTimeFormatter inputFormat = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss");
        LocalDate date = LocalDate.parse("11/27/2020 05:35:00", inputFormat);
        DateTimeFormatter outputFormat = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        System.out.println(date.format(outputFormat));
    }
}
