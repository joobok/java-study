package example.tree;

import example.tree.converter.TreeItemConverter;
import example.tree.converter.impl.TreeItemFromFile;
import example.tree.converter.impl.TreeItemToString01;
import example.tree.converter.impl.TreeItemToString02;
import example.tree.converter.impl.TreeItemToString03;
import example.tree.mock.MockItem;
import example.tree.mock.MockItemList;
import example.tree.mock.TreeItemFromMock;
import example.tree.view.TreeItemView;
import example.tree.view.impl.TreeItemConsoleView01;
import example.tree.view.impl.TreeItemConsoleView02;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

public class TreeItemTest {

    @Test
    public void FilesWalkTest() throws IOException {
        String rootPath = "D:\\문서";
        Stream<Path> walk = Files.walk(Paths.get(rootPath));
        walk.forEach(System.out::println);
    }

    @Test
    public void testFile() {
        String rootPath = "D:\\문서";
        TreeItem treeItem = new TreeItemFromFile().from(new File(rootPath));
        test(treeItem, new TreeItemConsoleView01(), new TreeItemToString01());
        test(treeItem, new TreeItemConsoleView01(), new TreeItemToString02());
        test(treeItem, new TreeItemConsoleView01(), new TreeItemToString03());
        test(treeItem, new TreeItemConsoleView02(), new TreeItemToString01());
        test(treeItem, new TreeItemConsoleView02(), new TreeItemToString02());
        test(treeItem, new TreeItemConsoleView02(), new TreeItemToString03());
    }

    @Test
    public void testMock() {
        List<MockItem> mockItems = MockItemList.getMockItemList();
        TreeItem treeItem = new TreeItemFromMock().from(mockItems);
        test(treeItem, new TreeItemConsoleView01(), new TreeItemToString01());
        test(treeItem, new TreeItemConsoleView01(), new TreeItemToString02());
        test(treeItem, new TreeItemConsoleView01(), new TreeItemToString03());
    }

    private void test(TreeItem treeItem, TreeItemView view, TreeItemConverter converter) {
        view.render(treeItem, converter);
        System.out.println(StringUtils.repeat("-", 50));
    }


    @Test
    public void testTreeSearch() {
        String rootPath = "D:\\문서";
        TreeItem treeItem = new TreeItemFromFile().from(new File(rootPath));
        //파일명 대소문자 구분없이 "policy" 나 "project" 를 포함한 것
        {
            new TreeItemSearch(treeItem)
                .setCaseSensitive(false)
                .setMatchRange(TreeItemSearch.MatchRange.NAME)
                .setMatchType(TreeItemSearch.MatchType.PART)
                .setMultiMode(TreeItemSearch.MultiMode.OR)
                .search("policy", "project")

                .getResult()
                .forEach(System.out::println);
        }
        System.out.println(StringUtils.repeat("-", 50));
        //파일명 대소문자 구분없이 "policy" 나 "project" 를 포함한 것 중 pdf 만.
        {
            new TreeItemSearch(treeItem)
                .setCaseSensitive(false)
                .setMatchRange(TreeItemSearch.MatchRange.NAME)
                .setMatchType(TreeItemSearch.MatchType.PART)
                .setMultiMode(TreeItemSearch.MultiMode.OR)
                .search("policy", "project")

                .setNextSearchMode(TreeItemSearch.NextSearchMode.FILTER)
                .setMatchRange(TreeItemSearch.MatchRange.EXT_NAME)
                .setMatchType(TreeItemSearch.MatchType.FULL)
                .search("pdf")

                .getResult()
                .forEach(System.out::println);
        }
        System.out.println(StringUtils.repeat("-", 50));
        //파일명 대소문자 구분하여 "Expense" 와 "Project" 두 단어를 다 포함하는 것 중 xlsx 만
        {
            new TreeItemSearch(treeItem)
                .setCaseSensitive(true)
                .setMatchRange(TreeItemSearch.MatchRange.NAME)
                .setMatchType(TreeItemSearch.MatchType.PART)
                .setMultiMode(TreeItemSearch.MultiMode.AND)
                .search("Expense", "Project")

                .setNextSearchMode(TreeItemSearch.NextSearchMode.FILTER)
                .setMatchRange(TreeItemSearch.MatchRange.EXT_NAME)
                .setMatchType(TreeItemSearch.MatchType.FULL)
                .search("xlsx")

                .getResult()
                .forEach(System.out::println);
        }
    }
}
