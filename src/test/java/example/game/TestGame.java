package example.game;

import example.game.baseball.BaseBallGame;
import example.game.baseball.ui.console.ConsoleUI;
import example.game.baseball.core.GameRule;
import example.game.baseball.ui.swing.SwingUI;

/**
 * System.in 으로 사용자 입력값 받는 것은 JUnit 테스트로 실행이 안된다...?
 */
public class TestGame {
    public static void main(String[] ar) {
        TestGame testGame = new TestGame();
        new Thread(() -> testGame.testSwing()).start();
        new Thread(() -> testGame.testConsole()).start();
    }

    public void testConsole() {
        ConsoleUI ui = new ConsoleUI();
        GameRule rule = new GameRule();
        BaseBallGame baseBallGame = new BaseBallGame(ui, rule);
        baseBallGame.start();
    }

    public void testSwing() {
        SwingUI ui = new SwingUI();
        GameRule rule = new GameRule();
        rule.setMatchingSize(3);
        BaseBallGame baseBallGame = new BaseBallGame(ui, rule);
        baseBallGame.start();
    }

}